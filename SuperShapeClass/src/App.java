public class App {
    public static void main(String[] args) throws Exception {
        //6. create shape 1 and shape2
        Shape shape1 = new Shape();
        Shape shape2 = new Shape("green", false);
        System.out.println(shape1.toString());
        System.out.println(shape2.toString());

        //7.create 3 objects circle 
        Circle circle1 = new Circle();
        Circle circle2 = new Circle(2.0);
        Circle circle3 = new Circle("green", false, 3.0);
        System.out.println(circle1.toString());
        System.out.println(circle2.toString());
        System.out.println(circle3.toString());

        //8.create 3 objects rectangle
        Rectangle rectangle1 = new Rectangle();
        Rectangle rectangle2 = new Rectangle(1.5, 2.5);
        Rectangle rectangle3 = new Rectangle("green", true, 1.5, 2.0);
        System.out.println(rectangle1.toString());
        System.out.println(rectangle2.toString());
        System.out.println(rectangle3.toString());

        //9.create 3 objects square
        Square square1 = new Square();
        Square square2 = new Square(1.5);
        Square square3 = new Square("green", true, 2.0);
        System.out.println(square1.toString());
        System.out.println(square2.toString());
        System.out.println(square3.toString());

        //10. area and perimeter of circle
        System.out.println("area of circle 1: " + circle1.getArea());
        System.out.println("area of circle 2: " + circle2.getArea());
        System.out.println("area of circle 3: " +circle3.getArea());

        System.out.println("perimeter of circle 1: " + circle1.getPerimeter());
        System.out.println("perimeter of circle 2: " +circle2.getPerimeter());
        System.out.println("perimeter of circle 3: " +circle3.getPerimeter());

         //11. area and perimeter of rectangle
         System.out.println("area of rectangle 1: " + rectangle1.getArea());
         System.out.println("area of rectangle 2: " + rectangle2.getArea());
         System.out.println("area of rectangle 3: " + rectangle3.getArea());
 
         System.out.println("perimeter of rectangle 1: " + rectangle1.getPerimeter());
         System.out.println("perimeter of rectangle 2: " + rectangle2.getPerimeter());
         System.out.println("perimeter of rectangle 3: " + rectangle3.getPerimeter());

         //12. area and perimeter of square
         System.out.println("area of square 1: " + square1.getArea());
         System.out.println("area of square 2: " + square2.getArea());
         System.out.println("area of square 3: " + square3.getArea());
 
         System.out.println("perimeter of square 1: " + square1.getPerimeter());
         System.out.println("perimeter of square 2: " + square2.getPerimeter());
         System.out.println("perimeter of square 3: " + square3.getPerimeter());

        
   }
}
